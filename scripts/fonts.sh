#!/bin/sh

sudo pacman -S --noconfirm ttf-fira-code

yay -S --noconfirm nerd-fonts-mononoki ttf-meslo-nerd-font-powerlevel10k
