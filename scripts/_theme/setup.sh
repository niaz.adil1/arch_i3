#!/bin/sh

pip install Pillow

sudo pacman -S --noconfirm imagemagick

mkdir -p $HOME/.config/theme_colors/themit

cp -R $BASE_DIR/scripts/_theme/theme_colors/. $HOME/.config/theme_colors/

git clone https://gitlab.com/niaz.adil1/themit.git $HOME/.config/theme_colors/themit

python $BASE_DIR/scripts/_theme/get_fonts.py
