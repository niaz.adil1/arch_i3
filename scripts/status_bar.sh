#!/bin/sh

case "$STATUS_BAR" in
  "I3_BLOCKS" ) sudo pacman -S --noconfirm i3blocks;;
  "POLYBAR" ) yay -S --noconfirm polybar;;
  * ) sudo pacman -S --noconfirm i3blocks;;
esac
