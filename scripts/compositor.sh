#!/bin/sh

sudo pacman -S --noconfirm picom

mkdir -p "$HOME/.config/picom"
cp /etc/xdg/picom.conf "$HOME/.config/picom/picom.conf"
