## Requirements

- Arch installed with minimal setup
- Network Manager installed
- Audio Server installed
- Logged in as the user

## Download
```
mkdir -p $HOME/.config

cd $HOME/.config

git clone https://gitlab.com/niaz.adil1/post_arch.git
```
## Run the script
```
cd post_arch

./install.sh
```
